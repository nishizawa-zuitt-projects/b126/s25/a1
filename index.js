/*
Activity:
	In Postman, create a new collection named s25-a1 and add the following requests
	to that collection:

		- GET all request to jsonplaceholder's todo resource
		- GET specific request for todo id 199 to jsonplaceholder's to do resource
		- POST request to jsonplaceholder's todo resource
		- PUT request for todo id 123 to jsonplaceholder's todo resource 
		- DELETE request for todo id 7 to jsonplaceholder's todo resource

	Make sure to test each request to see if it is working, and if it is working,
	make sure to properly save the request
*/